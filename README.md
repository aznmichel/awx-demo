# Playbooks Demo AWX

Ces playbooks ont été réalisés pour illustrer l'utilisation des workflows au sein de AWX.

## Prérequis

Dans le cadre de la démo AWX, voici les versions utilisées pour jouer ces différents playbooks:

*  Ansible version = 2.10.9
*  Python version = 3.8.2
*  AWX version = 19.1.0 
*  AWX CLI = 19.1.0

## Workflow illustré

![Processus de sauvegarde](images/demo.png)

