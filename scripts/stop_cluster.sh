#!/bin/bash

printf "SCRIPT ARRET DES SERVICES: \n"

while getopts :n: option
do
 case "${option}"
 in
 n) 
    NAME_PARAM=${OPTARG}
    ;;
 \?) 
    { printf '\e[1;31m%-6s\e[m \n' "Option invalide: -$OPTARG" >&2; exit 1; }
    ;;
 esac
done

if [[ ! "$NAME_PARAM" ]]; then
  printf '\e[1;31m%-6s\e[m \n' "ERROR: argument(s) requis invalide(s) (-n)"
  printf '\e[1;33m%-6s\e[m \n' "RAPPEL:"
  printf '\e[1;33m%-6s\e[m \n' "[-n = nom du cluster]"
  exit 1
fi

printf "==> Arrêt du cluster $NAME_PARAM... \n"
